# PEMOGRAMAN PLATFORM KHUSUS

## PRAKTIKUM PERTEMUAN 13 : ASYNCTASK

## Identitas

NAMA : M ALIF FARHAN

NIM : 222011401

KELAS : 3SI3

## ASYNCTAST

1. Buatlah laporan tangkapan layar pada kegiatan yang anda lakukan pada 13.4.
   a. tampilan aplikasi ketika pertama dibuka
   ![SS tampilan aplikasi ketika pertama kali dibuka](/SS/01.jpg "Dokumentasi")

   b. tampilan aplikasi sesaat ketika memencet Button download
   ![SS tampilan aplikasi sesaat ketika memencet Button download](/SS/02.jpg "Dokumentasi")

   c. tampilan aplikasi ketika gambar telah selesai di download
   ![SS tampilan aplikasi ketika gambar telah selesai di download](/SS/03.jpg "Dokumentasi")

2. Menurut informasi dari android developers, Kelas AsyncTask ini telah
   terdeprecated pada API level 30 sehingga disarankan menggunakanjava.util.concurrent saja. Ubahlah kode di atas dengan menggunakan
   kelas tersebut.

   Tampilan Kode Program yang diubah pada MainActivity.java
   ![SS Tampilan Kode Program yang diubah pada MainActivity.java](/SS/04.png "Dokumentasi")
